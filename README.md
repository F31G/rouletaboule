
### Consultation du projet

- Repo Git: https://gitlab.com/F31G/rouletaboule

- Surge: http://concours-rouletaboule.surge.sh/

- Maquette dans dossier "maquette" du projet "rouletaboule" sur Git

- URL: "    https://www.figma.com/file/g5WzrBPcVrcoVG1pKtdkOr/Concours-RouleTaBoule?node-id=0%   "
  (j'ai pu constater que du texte sautait lors de l'export en .pdf c'est pourquoi
  vous trouverez ci dessus le lien direct de ma maquette Figma)

_________________________________________________

## Contexte du projet

Le concours se déroulera à Carmaux le 12 juin prochain. Les participants devront s'inscrire au minimum un mois avant l'événement en envoyant un message à rouletaboule@cornomail.net et préciser les équipes complètes (2 ou 4 personnes majeures obligatoirement), ainsi que leur club d'affiliation, s'il en sont adhérant.

La charte graphique est assez libre : le projet doit être sur une base de vert et marron avec une police sans empattement. Le noir est interdit pour les textes, et une illustration doit être inclue. Bien entendu, la création doit être absolument magnifique et donner une envie irrésistible de venir à l'événement, qu'on soit joueur ou non :)

L'engagement est fixé à 10€ pour les licenciés, et 15€ pour les autres. La logistique sur place permettra de se restaurer et se désaltérer. Un espace sanitaire sera à disposition. Pas de distributeur de billet sur place.


## Modalités pédagogiques

Rendu avant ce soir 17h!

Ne seront évalués que les projets répondant aux attentes suivantes :

* respect de la deadline (avant 17h du jour courant, l'heure de Simplonline faisant foi) * dépôt Gitlab accessible (merci de bien vérifier ce point) * plus de 5 commits légitimes


## Critères de performance

Compétence 1 : Les maquettes sont au format numérique et font apparaitre l'ensemble des éléments présentés dans la demande client. Elles mettent en évidence l'adaptabilité du projet à tous types de supports numériques.

Compétence 2 : Les pages sont conformes aux maquettes, respectent la charte graphique imposée, ainsi que les bonnes pratiques en terme d'éco-conception, de performance et sécurité :

    ecoindex B minimum (ecoindex.fr)
    performance score 80% minimum (PageSpeed Insight, mobile ET ordinateur)
    accessibilité correcte (aucune erreur détectée par l'extension de navigateur Wave)
    indentation propre

A ces critères s'ajoutent les critères de performance du REAC pour les compétences ciblées.


## Modalités d'évaluation

Revue de code avec un formateur.

## Livrables

Un fichier README.md clair permet d'accéder facilement : * aux maquettes * à la version consultable en ligne du projet (hébergé sur surge.sh ou autre) * à une notice d'installation claire et exhaustive du projet